#include <stdlib.h>

#include "image.h"

#ifndef IMAGE_TRANSFORMER_TRANSFORMER_H
#define IMAGE_TRANSFORMER_TRANSFORMER_H

struct image rotate_image(struct image image);

#endif //IMAGE_TRANSFORMER_TRANSFORMER_H
