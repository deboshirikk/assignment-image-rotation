#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

struct pixel { uint8_t b, g, r; };

struct image {
    uint32_t width, height;
    struct pixel* data;
};

void free_image(struct image image);

#endif //IMAGE_TRANSFORMER_IMAGE_H
