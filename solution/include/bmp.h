#include  <stdint.h>

#include "image.h"

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H

#define BMP_FILE_SIGNATURE 0x4D42
#define BMP_SUPPORTED_BITS 24
#define BMP_INFO_SIZE 40
#define BMP_PLANES 1
#define BMP_COMPRESSION 0
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0

#pragma pack(push, 1)
struct bmp_header{
uint16_t bfType;
uint32_t bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t biHeight;
uint16_t biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t biClrImportant;
};
#pragma pack(pop)

//bool read_header(struct bmp_header* header, FILE* img);

enum STATUS_BMP_READ{
    BMP_READ_SUCCESS = 0,
    BMP_READ_FILE_READ_ERROR = 100,
    BMP_READ_INVALID_FILE = 101,
    BMP_READ_MEMORY_FAILED = 102,
};

enum STATUS_BMP_WRITE{
    BMP_WRITE_SUCCESS = 0,
    BMP_WRITE_ERROR = 200,
};

enum STATUS_BMP_READ read_bmp_image(struct image* image, FILE* file);
enum STATUS_BMP_WRITE save_bmp_image(const struct image image, FILE* file);


#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
