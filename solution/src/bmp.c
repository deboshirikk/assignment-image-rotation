#include <stdbool.h>
#include <stdio.h>

#include  "bmp.h"

static bool read_header(struct bmp_header* header, FILE* img){
    size_t count_read = fread(header, sizeof (struct bmp_header), 1, img);
    if (count_read != 1) {
        printf("Can't read header");
        return false;
    }
    return true;
}

static enum STATUS_BMP_READ check_bmp_header(const struct bmp_header bmpHeader){
    if(bmpHeader.bfType != BMP_FILE_SIGNATURE) return BMP_READ_INVALID_FILE;
    if(bmpHeader.biBitCount != BMP_SUPPORTED_BITS) return BMP_READ_INVALID_FILE;
    return BMP_READ_SUCCESS;
}

static uint32_t get_padding(uint32_t image_width){
    return (4 - image_width * sizeof (struct pixel) % 4) % 4;
}

static enum STATUS_BMP_READ read_pixels(struct image image, FILE* file){
    for(uint32_t i = 0; i < image.height; i++){
        if(fread(image.data + i * image.width, sizeof (struct pixel), image.width, file) != image.width) return BMP_READ_FILE_READ_ERROR;
        if(fseek(file, get_padding(image.width), SEEK_CUR)) return BMP_READ_FILE_READ_ERROR;
    }
    return BMP_READ_SUCCESS;
}

enum STATUS_BMP_READ read_bmp_image(struct image* image, FILE* file){
    if (file == NULL) return BMP_READ_FILE_READ_ERROR;

    struct bmp_header bmpHeader = {0};
    if(!read_header(&bmpHeader, file)) return BMP_READ_FILE_READ_ERROR;

    enum STATUS_BMP_READ bmp_check_result = check_bmp_header(bmpHeader);
    if(bmp_check_result) return bmp_check_result;

    // create image
    image->width = bmpHeader.biWidth;
    image->height = bmpHeader.biHeight;
    image->data = malloc(sizeof (struct pixel) * image->width * image->height);

    if (image->data == NULL) return BMP_READ_MEMORY_FAILED;

    enum STATUS_BMP_READ pixels_read_status = read_pixels(*image, file);
    if(pixels_read_status) return pixels_read_status;

    return BMP_READ_SUCCESS;
}

static struct bmp_header create_bmp_header(const struct image image){
    uint32_t image_size = sizeof (struct pixel) * image.height * (image.width + get_padding(image.width));

    struct bmp_header bmpHeader = {
            .bfType = BMP_FILE_SIGNATURE,
            .bfileSize = image_size + sizeof (struct bmp_header),
            .bOffBits = sizeof (struct bmp_header),
            .biSize = BMP_INFO_SIZE,
            .biWidth = image.width,
            .biHeight = image.height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_SUPPORTED_BITS,
            .biCompression = BMP_COMPRESSION,
            .biClrUsed = BMP_CLR_USED,
            .biClrImportant = BMP_CLR_IMPORTANT,
    };

    return bmpHeader;
}

static enum STATUS_BMP_WRITE write_pixels(struct image image, FILE* file){
    const uint32_t stupid_bytes = 0;
    for(uint32_t i = 0; i < image.height; i++){
        if(fwrite(image.data + i * image.width, sizeof (struct pixel), image.width, file) != image.width) return BMP_WRITE_ERROR;
        if(!fwrite(&stupid_bytes, 1, get_padding(image.width), file)) return BMP_WRITE_ERROR;
    }
    return BMP_WRITE_SUCCESS;
}

enum STATUS_BMP_WRITE save_bmp_image(const struct image image, FILE* file){
    if(file == NULL) return BMP_WRITE_ERROR;

    struct bmp_header header = create_bmp_header(image);
    // write header
    if(fwrite(&header, sizeof (struct bmp_header), 1, file) != 1) return BMP_WRITE_ERROR;

    enum STATUS_BMP_WRITE write_pixels_status = write_pixels(image, file);
    if(write_pixels_status) return write_pixels_status;

    return BMP_WRITE_SUCCESS;
}

