#include <stdint.h>
#include <stdlib.h>

#include "image.h"

struct image rotate_image(struct image image){
    struct image new_image = {
            .width = image.height,
            .height = image.width,
            .data = malloc(sizeof (struct pixel) * image.width * image.height)
    };

    if (new_image.data == NULL) return (struct image){0};

    for(uint32_t i = 0; i < image.height; i ++){
        for(uint32_t j = 0; j < image.width; j++){
            new_image.data[new_image.width * j + new_image.width - i - 1] = image.data[image.width * i + j];
        }
    }

    return new_image;
}
