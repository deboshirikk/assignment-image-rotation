#include <stdio.h>

#include "bmp.h"
#include "transformer.h"

int main( int argc, char** argv ) {

    if(argc < 3) {
        printf("Usage: image-transformer <input path> <output path>");
    }

    FILE* input_file = fopen(argv[1], "rb");
    struct image input_image = {0};

    enum STATUS_BMP_READ status_read =  read_bmp_image(&input_image, input_file);
    if(status_read) return status_read;

    struct image rotated_image = rotate_image(input_image);

    if(rotated_image.data == NULL)
        {free_image(input_image);
        free_image(rotated_image);

        fclose(input_file);
        return -1;}

    FILE* output_file = fopen(argv[2], "wb");
    enum STATUS_BMP_WRITE status_write = save_bmp_image(rotated_image, output_file);
    if(status_write) return status_write;

    free_image(input_image);
    free_image(rotated_image);

    fclose(input_file);
    fclose(output_file);

    return 0;
}
